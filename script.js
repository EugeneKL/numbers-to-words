//Initialize arrays for mapping place values
const singles = ["", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];

const teens = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];

const tens = ["", "Ten", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];


//Define functions for converting a particular range of numbers to words
function convertSingle(num){   
    return singles[num];
}


function convertTeens(num){
    return teens[(num-10)];
}


function convertTens(num){
    return tens[Math.floor(num/10)] + singles[(num%10)];
}


function convertHundreds(num){
    let hundredsValue = 0;
    let tensValue = 0;
    hundredsValue = singles[Math.floor(num/100)] + "Hundred";    
    if ((num%100)>=11 && (num%100)<=19){ 
        tensValue = teens[(num%100)-10];
    }
    else tensValue = tens[Math.floor((num%100)/10)] + singles[(num%10)];
    return hundredsValue + tensValue;
}


function convertThousand(num){
        return singles[(num/1000)] + "Thousand";
}


//Function for converting numbers to words
let words = [];

function numbersToWords(x){
    for (num=1; num<=x; num++){
        if (num>=1 && num <10){
            words.push(convertSingle(num));
        }
        else if (num>=10 && num<20){
            words.push(convertTeens(num));
        }
        else if (num>=20 && num<100){
            words.push(convertTens(num));
        }
        else if (num>=100 && num<1000){
            words.push(convertHundreds(num));
        }
        else if (num=1000){
            words.push(convertThousand(num));
        }
    }
    let wordstr = ""   
    for(let i=0; i<words.length; i++){
        wordstr += words[i]+ ", "
    }
    // return words;
    return wordstr;
}

//console.log(numbersToWords(1000));

//Display Output
let destination = document.getElementById('displayWords');

let array = document.createElement('div');
array.style.width = "800px";

let wordlist = document.createTextNode(numbersToWords(1000));

array.appendChild(wordlist);

destination.appendChild(array);